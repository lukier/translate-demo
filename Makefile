IMAGENAME=translate-demo-image
REPO=ewejsciowki.pl
IMAGEFULLNAME=${REPO}/${IMAGENAME}

build:
	@docker build -t ${IMAGEFULLNAME} .
	@kind load docker-image ${IMAGEFULLNAME}
all: build
