import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LanguageComponent} from "./language/language.component";
import {AppComponent} from "./app.component";
import {NoPageComponent} from "./no-page/no-page.component";

const routes: Routes = [
  {path: ':language', component: LanguageComponent},
  {path: '**', component: NoPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
