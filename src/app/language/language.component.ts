import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {TranslocoService} from "@ngneat/transloco";

@Component({
    selector: 'app-language',
    templateUrl: './language.component.html',
    styleUrls: ['./language.component.css']
})
export class LanguageComponent implements OnInit {
    lang: string = "";

    constructor(private route: ActivatedRoute, private translocoService: TranslocoService) {
    }

    ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.lang = params['language'];
            this.translocoService.setActiveLang(this.lang);

        })
    }

}
