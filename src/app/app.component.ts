import {Component} from '@angular/core';
import {TranslocoService} from '@ngneat/transloco';
import {FormControl} from '@angular/forms';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'translate-demo';
    number = 0;
    public countForm = new FormControl('');

    constructor(private translocoService: TranslocoService) {
    }
}
